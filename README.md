## NetSuite Transactions Inherit Classification

Application that makes Transaction lines inherit the native classification fields (Department and Class) from the body 
of the Transaction.