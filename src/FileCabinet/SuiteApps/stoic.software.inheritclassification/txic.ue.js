define([], function () {

    /**
     * Defaults the Department and Class of any transaction lines to the Department and Class from the body of the
     * record
     *
     * @exports txic/ue
     *
     * @copyright 2017 Stoic Software, LLC
     * @author Eric T Grubaugh <eric@stoic.software>
     *
     * @NApiVersion 2.x
     * @NModuleScope SameAccount
     * @NScriptType UserEventScript
     */
    var exports = {};

    /**
     * <code>beforeSubmit</code> event handler
     *
     * @governance 0
     *
     * @param context
     *        {Object}
     * @param context.newRecord
     *        {record} The new record being submitted
     * @param context.oldRecord
     *        {record} The old record before it was modified
     * @param context.type
     *        {UserEventType} The action type that triggered this event
     *
     * @return {void}
     *
     * @static
     * @function beforeSubmit
     */
    function beforeSubmit(context) {
        var bodyClass = context.newRecord.getValue({"fieldId": "class"});
        var bodyDepartment = context.newRecord.getValue({"fieldId": "department"});
        var lineCount = context.newRecord.getLineCount({"sublistId": "item"});

        if (!(bodyClass || bodyDepartment)) {
            return;
        }

        for (var i = 0; i < lineCount; i++) {
            if (bodyClass) {
                defaultField("class", bodyClass, i);
            }

            if (bodyDepartment) {
                defaultField("department", bodyDepartment, i);
            }
        }
    }

    /**
     * Ensures that the defaultValue is set in the provided fieldName on the given item line
     *
     * @governance 0
     *
     * @param fieldName {String} The ID of the field to default
     * @param defaultValue {String} The value to be set in the field
     * @param i {Number} The index of the line
     *
     * @returns {void}
     *
     * @private
     * @function defaultField
     */
    function defaultField(fieldName, defaultValue, i) {
        var currentValueIsEmpty = !(context.newRecord.getSublistValue({
            "sublistId": "item",
            "fieldId": fieldName,
            "line": i
        }));

        if (currentValueIsEmpty) {
            context.newRecord.setSublistValue({
                "sublistId": "item",
                "fieldId": fieldName,
                "line": i,
                "value": defaultValue
            });
        }
    }

    exports.beforeSubmit = beforeSubmit;
    return exports;
});
